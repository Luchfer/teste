package br.com.mastertech.cnppjservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CnpjserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CnpjserviceApplication.class, args);
	}

}
