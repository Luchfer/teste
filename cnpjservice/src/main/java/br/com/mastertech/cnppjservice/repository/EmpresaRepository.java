package br.com.mastertech.cnppjservice.repository;



import br.com.mastertech.cnppjservice.model.DadosEmpresa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmpresaRepository extends JpaRepository<DadosEmpresa,Long>{

}
