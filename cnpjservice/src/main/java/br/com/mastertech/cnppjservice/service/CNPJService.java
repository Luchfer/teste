package br.com.mastertech.cnppjservice.service;

import br.com.mastertech.cnppjservice.model.DadosEmpresa;
import br.com.mastertech.cnppjservice.repository.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CNPJService {
    @Autowired
    private CNPJProducer cnpjProducer;
    @Autowired
    private EmpresaRepository empresaRepository;

    public DadosEmpresa createEmpresa(DadosEmpresa dadosEmpresa){
        dadosEmpresa = empresaRepository.save(dadosEmpresa);
        cnpjProducer.enviarAoKafka(dadosEmpresa);
        return dadosEmpresa;
    }

}
