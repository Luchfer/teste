package br.com.mastertech.cnppjservice.model;

import br.com.mastertech.cnppjservice.request.DadosEmpresaRequest;
import br.com.mastertech.cnppjservice.response.DadosEmpresaResponse;
import org.springframework.stereotype.Component;

@Component
public class DadosEmpresaMapper {

    public DadosEmpresa toDadosEmpresa(DadosEmpresaRequest request){
        DadosEmpresa dadosEmpresa = new DadosEmpresa();
        dadosEmpresa.setCnpj(request.getCnpj());
        dadosEmpresa.setNomeEmpresa(request.getNomeEmpresa());
        return dadosEmpresa;
    }

    public DadosEmpresaResponse toResponse(DadosEmpresa dadosEmpresa){
        DadosEmpresaResponse response = new DadosEmpresaResponse();
        response.setId(dadosEmpresa.getId());
        response.setCnpj(dadosEmpresa.getCnpj());
        response.setNomeEmpresa(dadosEmpresa.getNomeEmpresa());
        return response;
    }

}
