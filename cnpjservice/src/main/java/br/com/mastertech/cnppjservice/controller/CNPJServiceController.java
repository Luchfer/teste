package br.com.mastertech.cnppjservice.controller;


import br.com.mastertech.cnppjservice.request.DadosEmpresaRequest;
import br.com.mastertech.cnppjservice.response.DadosEmpresaResponse;
import br.com.mastertech.cnppjservice.service.CNPJService;
import br.com.mastertech.cnppjservice.model.DadosEmpresa;
import br.com.mastertech.cnppjservice.model.DadosEmpresaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cnpj")
public class CNPJServiceController {
    @Autowired
    private CNPJService cnpjService;

    @Autowired
    private DadosEmpresaMapper dadosEmpresaMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DadosEmpresaResponse create(@RequestBody @Valid DadosEmpresaRequest request){
        DadosEmpresa dadosEmpresa = dadosEmpresaMapper.toDadosEmpresa(request);
        dadosEmpresa =  cnpjService.createEmpresa(dadosEmpresa);
        return dadosEmpresaMapper.toResponse(dadosEmpresa);
    }


}
