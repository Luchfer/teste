package br.com.mastertech.cnpjconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CnpjconsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CnpjconsumerApplication.class, args);
	}

}
