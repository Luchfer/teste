package br.com.mastertech.cnppjvalidateconsumer.client.dto;

public class DadosEmpresaClient {

    public String fantasia;
    public String status;
    public String porte;
    public String cnpj;
    public String situacao;

    public String getFantasia() {
        return fantasia;
    }

    public void setFantasia(String fantasia) {
        this.fantasia = fantasia;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPorte() {
        return porte;
    }

    public void setPort(String port) {
        this.porte = porte;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }
}
