package br.com.mastertech.cnppjvalidateconsumer.service;

import br.com.mastertech.cnppjservice.model.DadosEmpresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class dadosEmpresaConsumer {

    @Autowired
    private CNPJValidatorService cnpjValidatorService;

    @KafkaListener(topics = {"spec4-fernando-luchiari-1"}, groupId = "Luchiari")
    public void receber(@Payload DadosEmpresa dadosEmpresa) {
        cnpjValidatorService.validaCNPJ(dadosEmpresa);
    }
//,"spec4-fernando-luchiari-2","spec4-fernando-luchiari-3"
}
