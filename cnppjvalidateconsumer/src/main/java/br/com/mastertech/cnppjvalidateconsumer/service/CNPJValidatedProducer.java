package br.com.mastertech.cnppjvalidateconsumer.service;


import br.com.mastertech.cnppjservice.model.DadosEmpresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CNPJValidatedProducer {

    @Autowired
    private KafkaTemplate<String, DadosEmpresa> producer;

    public void enviarAoKafka(DadosEmpresa dadosEmpresa) {

        producer.send("spec4-fernando-luchiari-2", dadosEmpresa);
        /*producer.send("spec4-fernando-luchiari-2", logAcesso);
        producer.send("spec4-fernando-luchiari-3", logAcesso);

       spec4-fernando-luchiari-1
        spec4-fernando-luchiari-2
        spec4-fernando-luchiari-3*/
    }

}