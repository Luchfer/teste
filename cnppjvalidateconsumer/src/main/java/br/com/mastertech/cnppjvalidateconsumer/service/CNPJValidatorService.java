package br.com.mastertech.cnppjvalidateconsumer.service;

import br.com.mastertech.cnppjservice.model.DadosEmpresa;
import br.com.mastertech.cnppjvalidateconsumer.client.ReceitaFederalClient;
import br.com.mastertech.cnppjvalidateconsumer.client.dto.DadosEmpresaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CNPJValidatorService {
    @Autowired
    public ReceitaFederalClient receitaFederalClient;

    @Autowired
    private CNPJValidatedProducer cnpjValidatedProducer;


    public void validaCNPJ(DadosEmpresa dadosEmpresa){
        DadosEmpresaClient dadosEmpresaClient = null;
        try{
            dadosEmpresaClient = receitaFederalClient.validaEmpresa(dadosEmpresa.getCnpj());
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        if(dadosEmpresaClient.getStatus().equalsIgnoreCase("OK")){
            DadosEmpresa dados = new DadosEmpresa();
            dados.setNomeEmpresa(dadosEmpresa.getNomeEmpresa());
            dados.setCnpj(dadosEmpresa.getCnpj());
            dados.setFantasia(dadosEmpresaClient.getFantasia());
            dados.setStatus(dadosEmpresaClient.getStatus());
            dados.setPorte(dadosEmpresaClient.getPorte());
            dados.setSituacao(dadosEmpresaClient.getSituacao());
            cnpjValidatedProducer.enviarAoKafka(dados);
        }
    }
    
}
