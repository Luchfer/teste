package br.com.mastertech.cnppjvalidateconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class CnppjvalidateconsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CnppjvalidateconsumerApplication.class, args);
	}

}
