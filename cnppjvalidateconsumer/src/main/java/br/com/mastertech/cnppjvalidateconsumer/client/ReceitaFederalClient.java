package br.com.mastertech.cnppjvalidateconsumer.client;

import br.com.mastertech.cnppjvalidateconsumer.client.dto.DadosEmpresaClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep", url = "https://www.receitaws.com.br/")
public interface ReceitaFederalClient {

    @GetMapping("v1/cnpj/{cnpj}")
    public DadosEmpresaClient validaEmpresa(@PathVariable  String cnpj);
}
